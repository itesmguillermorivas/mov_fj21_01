package mx.itesm.fj1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class ExampleActivity extends AppCompatActivity {

    private TextView textView2;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example);

        // a way to retrieve the values in the intent -
        // first get the intent used to open this activity up
        Intent intent = getIntent();


        textView2 = findViewById(R.id.textView2);
        editText = findViewById(R.id.inputText);

        textView2.setText("My name is " +
                intent.getStringExtra("myName") +
                " and I'm " +
                intent.getIntExtra("myAge", -1));
    }

    public void goBack(View v){

        Intent i = new Intent();
        // EXTREMELY IMPORTANT: DON'T OPEN A NEW ACTIVITY IF YOU INTEND TO RETURN

        i.putExtra("greetings", editText.getText().toString());
        setResult(Activity.RESULT_OK, i);

        finish();
    }

}